import os
import subprocess
import uuid

from flask import Flask, request, send_from_directory
from pytube import YouTube

app = Flask(__name__)


@app.route('/', methods=('GET',))
def hello():
    subprocess.run('rm -f *.mp3', cwd="/home/dimtasev/", shell=True)
    subprocess.run('rm -f *.mp4', cwd="/home/dimtasev/", shell=True)

    url = request.args.get('url')
    yt = YouTube(url)
    title = request.args.get('title', yt.title)
    audio_stream = yt.streams.get_audio_only()
    if isinstance(audio_stream, list):
        audio_stream = audio_stream[0]

    file = audio_stream.download("/data")
    newname = f"{str(uuid.uuid4())[0:8]}"
    newfile = f"/home/dimtasev/{newname}"

    subprocess.run(f'mv "{file}" {newfile}.mp4', shell=True)
    subprocess.run(f'chown dimtasev:dimtasev {newfile}.mp4', shell=True)
    subprocess.run(f'chmod 777 {newfile}.mp4', shell=True)
    subprocess.run(f'su dimtasev -p -c "/snap/bin/ffmpeg -i {newname}.mp4 -b:a 320K {newname}.mp3"', shell=True, cwd='/home/dimtasev/')

    newfile = f"{newfile}.mp3"

    return send_from_directory(os.path.dirname(newfile), os.path.basename(newfile), as_attachment=True,
                               mimetype="audio/mp3", attachment_filename=f"{title}.mp3")



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=False)
